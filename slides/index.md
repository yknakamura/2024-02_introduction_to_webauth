---
title: Web認証技術入門
header: Web認証技術入門
marp: true
paginate: true
markdown.marp.enableHtml: true
style: |
  section.primary_section {
    background-size: 30%;
    background-position: right center;
    background-repeat: no-repeat;
  }

  section.primary_section h1 {
    font-size: 2.4rem;
    text-shadow: #FFF 0px 0 10px;
  }

  section.primary_section .author {
    font-size: 0.8rem;
    color: #999;
  }

  section.primary_section ul {
    font-size: 0.8rem;
    padding: 0;
  }

  section.primary_section li {
    list-style-type: none;
  }

  section::after {
    content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
  }

  .columns {
    display: grid;
    grid-template-columns: repeat(2, minmax(0, 1fr));
    gap: 1rem;
  }

  .bottom {
    position: sticky;
    top: 100vh;
  }
---

<!--
_header: ''
_class: primary_section
-->

# Web認証技術入門

<div class="author">
2024-02 by Yuki Nakamura
</div>

![Slides are here](images/qrcode.png)

- スライド: <https://yknakamura.gitlab.io/2024-02_introduction_to_webauth>
- リポジトリ: <https://gitlab.com/yknakamura/2024-02_introduction_to_webauth>

---

## 自己紹介

- 名前: Yuki Nakamura
- 守備範囲:
  - 主にバックエンド（APIなど）
  - フロントエンド（[React]）もやるようになった
    - 最近[Vue.js][vue]と[Svelte]もさわってみた
- 最近よく使う技術スタック
  - [TypeScript][]: プログラミング言語
  - [Next.js][nextjs]: Reactのフレームワーク
  - [Prisma][]: ORM
  - [NextAuth.js][nextauth]: 認証ライブラリ

[react]: https://react.dev/
[vue]: https://vuejs.org/
[svelte]: https://svelte.dev/
[typescript]: https://www.typescriptlang.org/
[nextjs]: https://nextjs.org/
[prisma]: https://www.prisma.io/
[nextauth]: https://next-auth.js.org/

---

## 今日話す内容

- 最近のWeb認証技術・認証事情について
- 最近注目のFIDO / Passkeyについて深掘り
- Keycloakで認証基盤を導入してみる

<div class="bottom">

後からじっくり見返せる資料にもなるように、リンクを沢山入れてあります。

</div>

---

## 今日ターゲットにする人

- Webアプリケーション開発者
- セキュリティに興味があるエンジニア
- Webサービスのプロジェクトに携わる人

<div class="bottom">

最近のWeb認証技術について「どういったものがあるのか」「どういう特徴があるのか」を知ってもらい、Webアプリの実装やプロダクトの方針決定に役立ててもらえたら幸いです。

</div>

---

# 最近のWeb認証技術・認証事情について

---

# 最近注目のFIDO / Passkeyについて深掘り

---

# Passkey導入事例

- [ヤフーが「パスキー」に対応、「Yahoo! JAPAN ID」認証で - ケータイ Watch](https://k-tai.watch.impress.co.jp/docs/news/1485723.html)（2023年3月）
- [ニンテンドーアカウント、パスキーに対応 スマホの生体認証でログイン - Impress Watch](https://www.watch.impress.co.jp/docs/news/1533907.html)（2023年9月）
- [Googleアカウントで「パスキー」が標準に、パスワードなしでログインできる - ケータイ Watch](https://k-tai.watch.impress.co.jp/docs/news/1538073.html)（2023年10月）
- [アマゾンがパスキー導入、パスワード不要でサインイン可能に - ケータイ Watch](https://k-tai.watch.impress.co.jp/docs/news/1541516.html)（2023年10月）
- [X(Twitter)がパスキーに対応。米国のiOS版から - PC Watch](https://pc.watch.impress.co.jp/docs/news/1563344.html)（2024年1月）

<div class="bottom">

➡Passkey対応サイト一覧: [Passkeys.directory](https://passkeys.directory/)

</div>

---

# Keycloakで認証基盤を導入してみる

---

# 参考情報

---

## 参考リンク

---

## 書籍紹介

---

# おわり

おつかれさまでした😃
