# 2024-02 技術勉強会「Web認証技術入門」

## スライドはここで見ることができます

<https://yknakamura.gitlab.io/2024-02_introduction_to_webauth>

※このスライドは[Marp](https://marp.app/)で作成されています。

## スライドの実行方法

### 依存ライブラリのインストール

まず、依存ライブラリをインストールします。

```sh
npm install
```

### ローカルサーバーの起動

スライド作成時など、ローカルでサーバーを起動してリアルタイム表示したい場合は、次のコマンドで起動します。

```sh
npm start
```

起動すると <http://localhost:8080> でアクセス出来ます。

ファイルを更新して保存すると、自動的にブラウザの内容も更新されます。

### 静的なHTMLとして出力する場合

```sh
npm run build
```

`public/`の中にファイルが生成されます。

### PDFとして出力する場合

前提：PDF出力の実行には、OSにChromium系のブラウザ（Google Chrome、 Chromium、Microsoft Edge）がインストールされている必要があります。

```sh
npm run pdf
```

`build/slide.pdf`に生成されます。

## このスライドをベースにして新しいスライドを作る方法

### 必要なもの

- Git
- Node.js v20
  - (Optional) Node用のバージョン管理ツール
    - `.node-version`が扱えるもの
    - `nodenv`, `nodebrew`, `asdf`, `mise`など

### 手順

- このリポジトリをcloneします
- `.git`を削除して、改めて自分のリポジトリを作成します
  - `git init`して、GitLabにリポジトリを作って、以下の手順でスライド書き直したら`push`します
- `package.json`の内容を書き換えます
  - `name`: スライドのパッケージ名
  - `description`: スライドの概要
  - `author`: 著者
  - `homepage`: スライドのページ（リポジトリのページ）
    - このURLからQRコードが生成されるようになっています
  - `repository`: スライドのリポジトリ（GitでアクセスするURLであること）
- `slides/`の中身を全部消します
  - `index.md`は消さずに中身を書き換えてもよいです
- `npm install`を実行して、依存ライブラリをインストールします
- `npm run generate:qrcode`を実行して、新しいQRコード画像を生成します
  - `package.json`の`homepage`を変更した際は、このコマンドで再生成します
- `README.md`内のスライドのリンクを書き換えます
- `slides/index.md`を改めて作って、スライドを書いていきます
  - `npm start`でサーバーを起動して、表示を確認しながら書きましょう
  - あるいは、VSCode用のMarp拡張[Marp for VS Code](https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode)を利用するとエディタ上でプレビューを表示しながら編集できます（おすすめ）

スライドの書き方などはMarpのドキュメントを参照してください。

## 参考

- [Marp: Markdown Presentation Ecosystem](https://marp.app/)
- [marp-team/marp: The entrance repository of Markdown presentation ecosystem](https://github.com/marp-team/marp/)
- [Marp CLI](https://github.com/marp-team/marp-cli)
- [Marpit](https://marpit.marp.app/)
  - [Marpit Markdown](https://marpit.marp.app/markdown)

### Marpについて

MarpはMarkdownでスライドが作成できるオープンソースのプレゼンテーションツールです。

HTMLの他にPDFやPowerPointでのエクスポートもできます。

#### 参考：MarpとかMarpitの関係

- Marpエコシステム
  - フレームワークとコアの部分
    - Marpit
    - Marp Core
  - ツール
    - Marp CLI
    - Marp for VS Code

#### HTMLを書く場合の注意点

スライドのMarkdown内に直接HTMLを書く場合は、以下の設定が必要になります。

- `.marprc.yml`に`html: true`を設定します
- スライド中のFront Matter等で`markdown.marp.enableHtml: true`を設定します
- VSCode拡張を使っている場合は`markdown.marp.enableHtml`を`true`にします

また、要素内のテキストをMarkdownとして解釈させたい場合は、前後に空行が必要になります。

よい例:

```markdown
<div class="foo">

Marpを使うとMarkdownで **簡単に** スライドが作れます。

</div>
```

ダメな例:

```markdown
<div class="foo">
Marpを使うとMarkdownで **簡単に** スライドが作れます。
</div>
```
